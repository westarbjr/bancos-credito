from rest_framework import serializers
from .models import Person, Debt


class DebtsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Debt
        fields = ('id', 'company', 'value', 'created_at')


class PersonSerializer(serializers.ModelSerializer):
    debits = DebtsSerializer(many=True, read_only=True)

    class Meta:
        model = Person
        fields = ('id', 'cpf', 'name', 'address', 'debits')

    def create(self, validated_data):
        debits_data = validated_data.pop('debits')
        person = Person.objects.create(**validated_data)
        for debits_data in debits_data:
            Debt.objects.create(person=person, **debits_data)

        return person
